package com.hcl.bank.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record TransactionDto(@NotNull(message = "fromAccount is required") long fromAccount,
		@NotNull(message = "toAccount is required") long toAccount,
		@NotNull(message = "amount is required") @Min(value = 1, message = "minimum amount must be 1") @Max(value = 10000, message = "maximum is 10000") double amount) {
}
