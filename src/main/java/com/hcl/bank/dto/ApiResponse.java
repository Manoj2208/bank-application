package com.hcl.bank.dto;

public record ApiResponse(String message, Long httpStatus) {

}
