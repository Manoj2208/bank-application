package com.hcl.bank.dto;

import lombok.Builder;

@Builder
public record AccountRecord(String accountHolder, long accountNumber, String ifsc, double balance, String bankName) {

}
