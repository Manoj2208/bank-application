package com.hcl.bank.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

@Builder
public record CustomerDto(@NotBlank(message = "firstName required") String firstName, String lastName,
		@NotBlank(message = "fatherName required") String fatherName,
		@NotBlank(message = "phone is required") @Pattern(regexp = "[6-9][\\d]{9}", message = "invalid phone number") String phone,
		@NotBlank(message = "aadhar is required") @Pattern(regexp = "[\\d]{12}", message = "invalid aadhar") String aadhar,
		@NotBlank(message = "specify password") @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$", message = "Password must contain atleast 1 uppercase letter, 1 lowercase letter,  1 special character, 1 number, Min 8 characters.") String password,
		@NotNull(message = "initial deposit is required") @Min(value = 1000, message = "initial deposit must equals or greater to 1000") double initialDeposit,
		@Valid AddressDto addressDto) {

}
