package com.hcl.bank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record AddressDto(@NotBlank(message = "lane required") String lane,
		@NotBlank(message = "city required") String city, @NotBlank(message = "state required") String state,
		@NotBlank(message = "country required") String country,
		@NotNull(message = "pincode is required") long pincode) {
}
