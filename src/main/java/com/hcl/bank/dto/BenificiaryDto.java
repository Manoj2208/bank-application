package com.hcl.bank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record BenificiaryDto(@NotNull(message = "accountNumber is required") long accountNumber,
		@NotBlank(message = "ifsc is required") String ifsc,
		@NotBlank(message = "bankName is required") String bankName,
		@NotBlank(message = "accountHolderName is required") String accountHolderName) {
}
