package com.hcl.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.BenificiaryAccount;
import com.hcl.bank.entity.Customer;

public interface BenificiaryAccountRepository extends JpaRepository<BenificiaryAccount, Long> {
	Optional<BenificiaryAccount> findByAccountNumberAndCustomer(long accountNumber, Customer customer);
}
