package com.hcl.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {
	Optional<Customer> findByAadharNo(String aadharNo);
}
