package com.hcl.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.Customer;

public interface AccountRepository extends JpaRepository<Account, Long> {
	Optional<Account> findByAccountNumber(long accountNumber);

	Optional<Account> findByCustomer(Customer customer);

	Optional<Account> findByAccountNumberAndCustomer(long fromAccount, Customer customer);
}
