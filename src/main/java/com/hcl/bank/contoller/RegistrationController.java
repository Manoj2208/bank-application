package com.hcl.bank.contoller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.CustomerDto;
import com.hcl.bank.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class RegistrationController {
	private final CustomerService customerService;

	@PostMapping("/register")
	public ResponseEntity<ApiResponse> registerCustomer(@Valid @RequestBody CustomerDto customerDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.register(customerDto));
	}
}
