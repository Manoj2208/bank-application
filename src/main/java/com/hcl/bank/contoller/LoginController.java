package com.hcl.bank.contoller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.dto.AccountRecord;
import com.hcl.bank.service.CustomerService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class LoginController {
	private final CustomerService customerService;

	@PutMapping("/login")
	public ResponseEntity<AccountRecord> login(@Valid @NotBlank(message = "customerId required")@RequestParam String customerId,
			@Valid @NotBlank(message = "password required") @RequestParam String password) {
		return ResponseEntity.status(HttpStatus.OK).body(customerService.login(customerId, password));
	}
}
