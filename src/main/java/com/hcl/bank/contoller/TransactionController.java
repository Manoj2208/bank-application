package com.hcl.bank.contoller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.TransactionDto;
import com.hcl.bank.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController {
	private final TransactionService transactionService;

	@PostMapping("/{customerId}")
	public ResponseEntity<ApiResponse> fundTransfer(@PathVariable String customerId,
			@Valid @RequestBody TransactionDto transactionDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(transactionService.fundTransfer(customerId, transactionDto));
	}
}
