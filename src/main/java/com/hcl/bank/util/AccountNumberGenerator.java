package com.hcl.bank.util;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Set;

public class AccountNumberGenerator {

	public static long generateAccountNumber() {
		Set<Long> generatedNumbers = new HashSet<>();
		SecureRandom secureRandom = new SecureRandom();
		long accountNumber;

		do {
			// to Generate a random number of 10 digits
			accountNumber = secureRandom.nextLong();
			accountNumber = Math.abs(accountNumber % 9_000_000_000L) + 1_000_000_000L;
		} while (!generatedNumbers.add(accountNumber)); // to Check if the number is unique

		return accountNumber;
	}

	private AccountNumberGenerator() {
		super();
	}

}
