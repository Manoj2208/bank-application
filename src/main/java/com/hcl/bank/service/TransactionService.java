package com.hcl.bank.service;

import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.TransactionDto;

import jakarta.validation.Valid;

public interface TransactionService {

	ApiResponse fundTransfer(String customerId, @Valid TransactionDto transactionDto);

}
