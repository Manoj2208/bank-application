package com.hcl.bank.service;

import com.hcl.bank.dto.AccountRecord;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.BenificiaryDto;
import com.hcl.bank.dto.CustomerDto;

public interface CustomerService {

	ApiResponse register(CustomerDto customerDto);

	AccountRecord login(String customerId, String password);

	ApiResponse addBeneficiary(String customerId, BenificiaryDto benificiaryDto);

}
