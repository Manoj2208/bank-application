package com.hcl.bank.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.bank.dto.AccountRecord;
import com.hcl.bank.dto.AddressDto;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.BenificiaryDto;
import com.hcl.bank.dto.CustomerDto;
import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.Address;
import com.hcl.bank.entity.BenificiaryAccount;
import com.hcl.bank.entity.Customer;
import com.hcl.bank.exception.ResourceConflictExists;
import com.hcl.bank.exception.ResourceNotFound;
import com.hcl.bank.exception.UnauthorizedUser;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.BenificiaryAccountRepository;
import com.hcl.bank.repository.CustomerRepository;
import com.hcl.bank.service.CustomerService;
import com.hcl.bank.util.AccountNumberGenerator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {
	private final CustomerRepository customerRepository;
	private final AccountRepository accountRepository;
	private final BenificiaryAccountRepository benificiaryAccountRepository;
	@Value("${bank.app.ifsc}")
	private String ifsc;
	@Value("${bank.app.name}")
	private String bankName;

	@Override
	@Transactional
	public ApiResponse register(CustomerDto customerDto) {
		Optional<Customer> customer = customerRepository.findByAadharNo(customerDto.aadhar());
		if (customer.isPresent()) {
			log.error("Customer Already Registered causes ResourceConflictExists");
			throw new ResourceConflictExists("Customer already registered");
		}
		String customerId = UUID.randomUUID().toString();
		Customer customer2 = Customer.builder().customerId(customerId).firstName(customerDto.firstName())
				.lastName(customerDto.lastName()).fatherName(customerDto.fatherName()).aadharNo(customerDto.aadhar())
				.contactNo(customerDto.phone()).password(customerDto.password())
				.address(getAddress(customerDto.addressDto())).build();
		log.info("customer persisted to db");
		customerRepository.save(customer2);
		long accountNumber = AccountNumberGenerator.generateAccountNumber();
		Account account = Account.builder().accountNumber(accountNumber).balance(customerDto.initialDeposit())
				.bankName(bankName).ifsc(ifsc).customer(customer2).build();
		log.info("account for the customer configured");
		accountRepository.save(account);
		log.info("Customer registered for the bank application");
		return new ApiResponse(String.format("Customer :%s registered successfully", customerId), 201l);
	}

	private Address getAddress(AddressDto addressDto) {
		return Address.builder().city(addressDto.city()).country(addressDto.country()).lane(addressDto.lane())
				.state(addressDto.state()).pincode(addressDto.pincode()).build();
	}

	@Override
	public AccountRecord login(String customerId, String password) {
		log.warn("Invalid Customer-Id causes to ResourceNotFound Exception");
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFound("Customer with this id not exists"));
		if (customer.getPassword().equals(password)) {
			customer.setLoggedIn(true);
			customerRepository.save(customer);
			log.warn("If there is no account for customer causes to ResourceNotFound");
			Account account = accountRepository.findByCustomer(customer)
					.orElseThrow(() -> new ResourceNotFound("No account for Customer Exists"));
			log.info("Customer logged in and account details found");
			return new AccountRecord(customer.getFirstName(), account.getAccountNumber(), account.getIfsc(),
					account.getBalance(), account.getBankName());

		} else {
			log.error("Password didnt match(Unauthorized customer)");
			throw new UnauthorizedUser("Please Enter valid Password");
		}
	}

	@Override
	public ApiResponse addBeneficiary(String customerId, BenificiaryDto benificiaryDto) {
		log.warn("Invalid Customer-Id causes  ResourceNotFound Exception");
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFound("Customer with this id does not exists"));
		if (customer.isLoggedIn()) {
			Optional<BenificiaryAccount> account = benificiaryAccountRepository
					.findByAccountNumberAndCustomer(benificiaryDto.accountNumber(), customer);
			if (account.isPresent()) {
				log.error("Duplicate Benificiary Addition caused ResourceConflictExists");
				throw new ResourceConflictExists("Benificiary account already present");
			}
			BenificiaryAccount benificiaryAccount = BenificiaryAccount.builder()
					.accountHolderName(benificiaryDto.accountHolderName()).bankName(benificiaryDto.bankName())
					.accountNumber(benificiaryDto.accountNumber()).ifsc(benificiaryDto.ifsc()).customer(customer)
					.build();
			benificiaryAccountRepository.save(benificiaryAccount);
			log.info("benificiary for the customer configured");
			return new ApiResponse("Benificiary Account added", 201l);

		} else {
			log.error("Unauthorized customer(login to add benificiary)");
			throw new UnauthorizedUser("Login to add Benificiary Details");
		}

	}

}
