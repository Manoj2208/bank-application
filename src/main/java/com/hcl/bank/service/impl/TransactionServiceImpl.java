package com.hcl.bank.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.TransactionDto;
import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.BenificiaryAccount;
import com.hcl.bank.entity.Customer;
import com.hcl.bank.entity.Transaction;
import com.hcl.bank.exception.InsufficientFundException;
import com.hcl.bank.exception.ResourceNotFound;
import com.hcl.bank.exception.UnauthorizedUser;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.BenificiaryAccountRepository;
import com.hcl.bank.repository.CustomerRepository;
import com.hcl.bank.repository.TransactionRepository;
import com.hcl.bank.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {
	private final CustomerRepository customerRepository;
	private final TransactionRepository transactionRepository;
	private final AccountRepository accountRepository;
	private final BenificiaryAccountRepository benificiaryAccountRepository;

	@Override
	@Transactional
	public ApiResponse fundTransfer(String customerId, @Valid TransactionDto transactionDto) {
		log.warn("Invalid Customer-Id causes to ResourceNotFound Exception");
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFound(String.format("Customer Not found with id:%s", customerId)));
		if (customer.isLoggedIn()) {
			log.warn("Invalid sourceAccountNumber causes ResourceNotFound Exception");
			Account fromAcc = accountRepository.findByAccountNumberAndCustomer(transactionDto.fromAccount(), customer)
					.orElseThrow(() -> new ResourceNotFound(
							String.format("Account with accountNumber:%d NotFound", transactionDto.fromAccount())));
			log.warn("Invalid targetAccountNumber causes ResourceNotFound Exception");
			BenificiaryAccount toAcc = benificiaryAccountRepository
					.findByAccountNumberAndCustomer(transactionDto.toAccount(), customer)
					.orElseThrow(() -> new ResourceNotFound(String
							.format("Benificiary Account with accountNumber:%d NotFound", transactionDto.toAccount())));
			if (fromAcc.getBalance() >= transactionDto.amount()) {
				fromAcc.setBalance(fromAcc.getBalance() - transactionDto.amount());
				toAcc.setBalance(toAcc.getBalance() + transactionDto.amount());
				accountRepository.save(fromAcc);
				benificiaryAccountRepository.save(toAcc);
				Transaction transaction = Transaction.builder().fromAccount(transactionDto.fromAccount())
						.toAccount(transactionDto.toAccount()).amount(transactionDto.amount()).build();
				transactionRepository.save(transaction);

			} else {
				log.error("Insufficient Funds in Source account");
				throw new InsufficientFundException();
			}
		} else {
			log.error("Unauthorized(customer not logged in)");
			throw new UnauthorizedUser();
		}
		log.info("Fund transferred from source account to target account");
		return new ApiResponse("Transaction succes", 201l);
	}
}
