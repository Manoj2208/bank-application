package com.hcl.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.dto.AccountRecord;
import com.hcl.bank.dto.AddressDto;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.BenificiaryDto;
import com.hcl.bank.dto.CustomerDto;
import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.Address;
import com.hcl.bank.entity.BenificiaryAccount;
import com.hcl.bank.entity.Customer;
import com.hcl.bank.exception.ResourceConflictExists;
import com.hcl.bank.exception.ResourceNotFound;
import com.hcl.bank.exception.UnauthorizedUser;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.BenificiaryAccountRepository;
import com.hcl.bank.repository.CustomerRepository;
import com.hcl.bank.service.impl.CustomerServiceImpl;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private BenificiaryAccountRepository benificiaryAccountRepository;
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;

	@Test
	void testRegister() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(false).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		AddressDto addressDto = AddressDto.builder().city("string").country("string").state("string").pincode(577001)
				.build();
		CustomerDto customerDto = CustomerDto.builder().aadhar("587164123035").phone("9110810596")
				.fatherName("mallikarjun").firstName("Manoj").lastName("kumar mk").password("Manoj@12345")
				.addressDto(addressDto).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		Mockito.when(customerRepository.findByAadharNo(customerDto.aadhar())).thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		ApiResponse apiResponse = customerServiceImpl.register(customerDto);
		assertNotNull(apiResponse);

		assertEquals(201, apiResponse.httpStatus());
	}

	@Test
	void testRegisterNegative() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(false).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		AddressDto addressDto = AddressDto.builder().city("string").country("string").state("string").pincode(577001)
				.build();
		CustomerDto customerDto = CustomerDto.builder().aadhar("587164123035").phone("9110810596")
				.fatherName("mallikarjun").firstName("Manoj").lastName("kumar mk").password("Manoj@12345")
				.addressDto(addressDto).build();
		Mockito.when(customerRepository.findByAadharNo(customerDto.aadhar())).thenReturn(Optional.of(customer));
		assertThrows(ResourceConflictExists.class, () -> customerServiceImpl.register(customerDto));
	}

	@Test
	void testLogin() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Mockito.when(accountRepository.findByCustomer(customer)).thenReturn(Optional.of(account));
		AccountRecord accountResponse = customerServiceImpl.login("86392620-c76a-430b-9afd-ea2de05ff30e",
				"Manoj@12345");
		assertNotNull(accountResponse);

	}

	@Test
	void testLoginCustomerNotExists() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> customerServiceImpl.login("86392620-c76a-430b-9afd-ea2de05f", "nfgg@1245"));

	}

	@Test
	void testLoginIncorrectPassword() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		assertThrows(UnauthorizedUser.class,
				() -> customerServiceImpl.login("86392620-c76a-430b-9afd-ea2de05ff30e", "nfgg@1245"));

	}

	@Test
	void testLoginAccountNotExists() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByCustomer(customer)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> customerServiceImpl.login("86392620-c76a-430b-9afd-ea2de05ff30e", "Manoj@12345"));
	}

	@Test
	void testAddBenificiary() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();

		BenificiaryAccount benificiaryAccount = BenificiaryAccount.builder().accountHolderName("Nadiya")
				.accountNumber(76528348).balance(4500).bankName("Axis Bank").customer(customer).build();
		BenificiaryDto benificiaryDto = BenificiaryDto.builder().accountHolderName("Saniya").accountNumber(798547645)
				.bankName("Axis").build();

		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(benificiaryDto.accountNumber(),
				customer)).thenReturn(Optional.empty());
		Mockito.when(benificiaryAccountRepository.save(benificiaryAccount)).thenReturn(benificiaryAccount);

		ApiResponse apiResponse = customerServiceImpl.addBeneficiary("86392620-c76a-430b-9afd-ea2de05ff30e",
				benificiaryDto);
		assertNotNull(apiResponse);

	}

	@Test
	void testAddBenificiaryIncorrectCustomer() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		BenificiaryDto benificiaryDto = BenificiaryDto.builder().accountHolderName("Saniya").accountNumber(798547645)
				.bankName("Axis").build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> customerServiceImpl.addBeneficiary("86392620-c76a-430b-9afd-ea2de0e", benificiaryDto));

	}

	@Test
	void testAddBenificiaryNotLoggedIn() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(false).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		BenificiaryDto benificiaryDto = BenificiaryDto.builder().accountHolderName("Saniya").accountNumber(798547645)
				.bankName("Axis").build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		assertThrows(UnauthorizedUser.class,
				() -> customerServiceImpl.addBeneficiary("86392620-c76a-430b-9afd-ea2de05ff30e", benificiaryDto));

	}

	@Test
	void testBenificiaryDuplicate() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		BenificiaryAccount account = BenificiaryAccount.builder().id(1).accountHolderName("Saniya")
				.accountNumber(798547645).bankName("Axis").ifsc("Axis123").balance(0).customer(customer).build();
		BenificiaryDto benificiaryDto = BenificiaryDto.builder().accountHolderName("Saniya").accountNumber(798547645)
				.bankName("Axis").build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(798547645, customer))
				.thenReturn(Optional.of(account));
		assertThrows(ResourceConflictExists.class,
				() -> customerServiceImpl.addBeneficiary("86392620-c76a-430b-9afd-ea2de05ff30e", benificiaryDto));
	}

}
