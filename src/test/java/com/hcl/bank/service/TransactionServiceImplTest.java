package com.hcl.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.TransactionDto;
import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.Address;
import com.hcl.bank.entity.BenificiaryAccount;
import com.hcl.bank.entity.Customer;
import com.hcl.bank.entity.Transaction;
import com.hcl.bank.exception.InsufficientFundException;
import com.hcl.bank.exception.ResourceNotFound;
import com.hcl.bank.exception.UnauthorizedUser;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.BenificiaryAccountRepository;
import com.hcl.bank.repository.CustomerRepository;
import com.hcl.bank.repository.TransactionRepository;
import com.hcl.bank.service.impl.TransactionServiceImpl;

@ExtendWith(SpringExtension.class)
class TransactionServiceImplTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private TransactionRepository transactionRepository;
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private BenificiaryAccountRepository benificiaryAccountRepository;

	@InjectMocks
	private TransactionServiceImpl transactionServiceImpl;

	@Test
	void testFundTransfer() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		BenificiaryAccount benificiaryAccount = BenificiaryAccount.builder().id(1).accountHolderName("Nadiya")
				.accountNumber(1234567).balance(0).bankName("sdsa").customer(customer).build();
		Transaction transaction = Transaction.builder().transactionId(1).fromAccount(account.getAccountNumber())
				.toAccount(benificiaryAccount.getAccountNumber()).amount(123).transactionTime(LocalDateTime.now())
				.build();
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(account.getAccountNumber())
				.toAccount(benificiaryAccount.getAccountNumber()).amount(123).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumberAndCustomer(account.getAccountNumber(), customer))
				.thenReturn(Optional.of(account));
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(benificiaryAccount.getAccountNumber(),
				customer)).thenReturn(Optional.of(benificiaryAccount));
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Mockito.when(benificiaryAccountRepository.save(benificiaryAccount)).thenReturn(benificiaryAccount);
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		ApiResponse apiResponse = transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e",
				transactionDto);
		assertNotNull(apiResponse);
		assertEquals(201, apiResponse.httpStatus());
	}

	@Test
	void testFundTransfer2() {
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(1).toAccount(2).amount(123).build();
		Mockito.when(customerRepository.findById("86392620-c76a-430b-9afd-ea2de05ff30e")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto));

	}

	@Test
	void testFundTransfer3() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(false).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(1).toAccount(2).amount(123).build();
		Mockito.when(customerRepository.findById("86392620-c76a-430b-9afd-ea2de05ff30e"))
				.thenReturn(Optional.of(customer));
		assertThrows(UnauthorizedUser.class,
				() -> transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto));

	}

	@Test
	void testFundTransfer4() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(account.getAccountNumber()).toAccount(2)
				.amount(123).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumberAndCustomer(account.getAccountNumber(), customer))
				.thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto));

	}

	@Test
	void TestFundTransfer5() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		BenificiaryAccount benificiaryAccount = BenificiaryAccount.builder().id(2).accountHolderName("Nadiya")
				.accountNumber(1234567).balance(0).bankName("sdsa").customer(customer).build();
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(account.getAccountNumber())
				.toAccount(benificiaryAccount.getAccountNumber()).amount(123).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumberAndCustomer(account.getAccountNumber(), customer))
				.thenReturn(Optional.of(account));
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(benificiaryAccount.getAccountNumber(),
				customer)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto));

	}

	@Test
	void testFundTransfer6() {
		Address address = Address.builder().addressId(1).city("string").country("string").state("string")
				.pincode(577001).build();
		Customer customer = Customer.builder().customerId("86392620-c76a-430b-9afd-ea2de05ff30e")
				.aadharNo("587164123035").contactNo("9110810596").fatherName("mallikarjun").firstName("Manoj")
				.lastName("kumar mk").loggedIn(true).password("Manoj@12345").registeredAt(LocalDateTime.now())
				.address(address).build();
		Account account = Account.builder().accountId(1).accountNumber(1234543).balance(1000).bankName("dwid")
				.customer(customer).ifsc("sds").build();
		BenificiaryAccount benificiaryAccount = BenificiaryAccount.builder().id(1).accountHolderName("Nadiya")
				.accountNumber(1234567).balance(0).bankName("sdsa").customer(customer).build();
		TransactionDto transactionDto = TransactionDto.builder().fromAccount(account.getAccountNumber())
				.toAccount(benificiaryAccount.getAccountNumber()).amount(1230).build();
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumberAndCustomer(account.getAccountNumber(), customer))
				.thenReturn(Optional.of(account));
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(benificiaryAccount.getAccountNumber(),
				customer)).thenReturn(Optional.of(benificiaryAccount));
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Mockito.when(benificiaryAccountRepository.save(benificiaryAccount)).thenReturn(benificiaryAccount);
		assertThrows(InsufficientFundException.class,
				() -> transactionServiceImpl.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto));
	}

}
