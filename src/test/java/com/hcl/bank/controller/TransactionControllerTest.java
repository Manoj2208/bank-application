package com.hcl.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.contoller.TransactionController;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.TransactionDto;
import com.hcl.bank.service.TransactionService;

@ExtendWith(SpringExtension.class)
class TransactionControllerTest {
	@Mock
	private TransactionService transactionService;
	@InjectMocks
	private TransactionController transactionController;

	@Test
	void testTransaction() {
		TransactionDto transactionDto = new TransactionDto(1234567890,2345678901l,123);
		ApiResponse apiResponse = new ApiResponse("Transaction succes",201l);
		Mockito.when(transactionService.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto))
				.thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = transactionController
				.fundTransfer("86392620-c76a-430b-9afd-ea2de05ff30e", transactionDto);
		assertNotNull(responseEntity);
		assertEquals("Transaction succes", responseEntity.getBody().message());
		assertEquals(201, responseEntity.getBody().httpStatus());

	}
}
