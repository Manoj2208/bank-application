package com.hcl.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.contoller.BenificiaryController;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.BenificiaryDto;
import com.hcl.bank.service.CustomerService;

@ExtendWith(SpringExtension.class)
class BenificiaryControllerTest {
	@Mock
	private CustomerService customerService;
	@InjectMocks
	private BenificiaryController benificiaryController;

	@Test
	void testAddBenificiary() {
		BenificiaryDto benificiaryDto = new BenificiaryDto( 798547645, "Axis1234","AXIS","Saniya");
		ApiResponse apiResponse = new ApiResponse("Benificiary Account added",201l);
		Mockito.when(customerService.addBeneficiary("86392620-c76a-430b-9afd-ea2de05ff30e", benificiaryDto))
				.thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = benificiaryController
				.addBenificiary("86392620-c76a-430b-9afd-ea2de05ff30e", benificiaryDto);
		assertNotNull(responseEntity);
		assertEquals(201, responseEntity.getBody().httpStatus());
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals("Benificiary Account added", responseEntity.getBody().message());
	}
}
