package com.hcl.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.contoller.LoginController;
import com.hcl.bank.dto.AccountRecord;
import com.hcl.bank.service.CustomerService;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {
	@Mock
	private CustomerService customerService;
	@InjectMocks
	private LoginController loginController;

	@Test
	void testLogin() {
		AccountRecord accountResponse = new AccountRecord("Manoj", 1234567890, "CNRB001064", 2000, "CANARA");
		Mockito.when(customerService.login("86392620-c76a-430b-9afd-ea2de05ff30e", "Manoj@12345"))
				.thenReturn(accountResponse);
		ResponseEntity<AccountRecord> responseEntity = loginController.login("86392620-c76a-430b-9afd-ea2de05ff30e",
				"Manoj@12345");
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

	}
}
