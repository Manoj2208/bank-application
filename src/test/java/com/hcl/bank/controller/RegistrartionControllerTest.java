package com.hcl.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.bank.contoller.RegistrationController;
import com.hcl.bank.dto.AddressDto;
import com.hcl.bank.dto.ApiResponse;
import com.hcl.bank.dto.CustomerDto;
import com.hcl.bank.service.CustomerService;

@ExtendWith(SpringExtension.class)
class RegistrartionControllerTest {
	@Mock
	private CustomerService customerService;
	@InjectMocks
	private RegistrationController registrationController;

	@Test
	void testRegistration() {
		AddressDto addressDto = new AddressDto("SKP Road", "Banglore", "Karnataka", "India", 577001);
		CustomerDto customerDto = new CustomerDto("Manoj","kumar mk","mallikarjun","9110810596","587164123035","Manoj@12345",2000,addressDto);
		ApiResponse apiResponse =new ApiResponse("Registration Completed",201l);
		Mockito.when(customerService.register(customerDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = registrationController.registerCustomer(customerDto);
		assertNotNull(responseEntity);
		assertEquals(201, responseEntity.getBody().httpStatus());
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

	}
}
