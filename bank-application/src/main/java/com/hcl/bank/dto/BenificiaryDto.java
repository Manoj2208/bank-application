package com.hcl.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BenificiaryDto {
	private long accountNumber;
	private long ifsc;
	private String bankName;
	private String accountHolderName;
}
