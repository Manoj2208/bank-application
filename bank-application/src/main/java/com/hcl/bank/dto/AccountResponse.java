package com.hcl.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {
	private String accountHolder;
	private long accountNumber;
	private String ifsc;
	private double balance;
	private String bankName;
	
}
