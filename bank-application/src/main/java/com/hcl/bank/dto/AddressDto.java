package com.hcl.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressDto {
	private String lane;
	private String city;
	private String state;
	private String country;
	private long pincode;
}
