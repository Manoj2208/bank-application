package com.hcl.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	private String firstName;
	private String lastName;
	private String fatherName;
	private String phone;
	private String aadhar;
	private String password;
	private double initialDeposit;
	private AddressDto addressDto;

}
