package com.hcl.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.Account;

public interface AccountRepository extends JpaRepository<Account,Long> {

}
