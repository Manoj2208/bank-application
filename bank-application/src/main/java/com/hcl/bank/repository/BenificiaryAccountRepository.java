package com.hcl.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.BenificiaryAccount;

public interface BenificiaryAccountRepository extends JpaRepository<BenificiaryAccount, Long> {

}
