package com.hcl.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.entity.Address;

public interface AddressRepository extends JpaRepository<Address,Long> {

}
